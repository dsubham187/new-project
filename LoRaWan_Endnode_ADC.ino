/*!
  * ----------------------------------------------------------------------
  * Copyright (c) 2022, System Level Solutions (India) Pvt. Ltd.
  * ----------------------------------------------------------------------
  *      Purpose:   This file is used to send battery voltage on lora server
  *      Package:   STM32WLE5JCI
  *      File name: LoRaWan_Endnode_ADC.ino
  * ----------------------------------------------------------------------
  */


/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <LoRaSTM32WL_Lib.h>
#include <SoftwareSerial.h>
#include "./src/main.h"
#include "./src/sys_app.h"
#include "./src/lora_app.h"
#include "./src/LoRaLib.h"
/***********************************************************************
 * CONSTANTS
 **********************************************************************/
#define  RADIO_TX_BUFFER_SIZE      242U
#define  BAUD_RATE                 115200U
#define  SERIAL_STABLE_DELAY       500U
#define  MAX_VAL                   16U
#define  ADC__RESOLUTION           12
#define  ADC_PIN                   PB_1
#define  SHIFT_8                   8U


/*!
 * Defines the application data transmission duty cycle. 300s, value in [ms].
 */
#define APP_TX_DUTYCYCLE                            300000

/*!
 * LoRaWAN default activation type
 */
#define LORAWAN_DEFAULT_ACTIVATION_TYPE             LORAWAN_ACTIVATION_TYPE_OTAA // or LORAWAN_ACTIVATION_TYPE_ABP

/*!
 * Time to send Link check request command
 */
#define LCR_CMD_TIME                                3600000    //3600 seconds

/*!
 * STATIC_DEVICE_ID_KEY macro will be 1 if user want to use below IDs
 */
#define   STATIC_DEVICE_ID_KEY                      0U

/*!
 * Device ID
 */
#define   DEVICE_ID                                 {0x00, 0x80, 0xe1, 0x15, 0x00, 0x00, 0x42, 0x58}

/*!
 * Application key
 */
#define   APPLICATION_KEY                           {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c}

/*!
 * Application session key
 */
#define   APPLICATION_SESSION_KEY                   {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c}

/*!
 * Network session key
 */
#define   NETWORK_SESSION_KEY                       {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c}

/***********************************************************************
 * PUBLIC FUNCTIONS
 **********************************************************************/
void setup() {
  // put your setup code here, to run once:

  HAL_Init();
  SystemClock_Config();
  analogReadResolution(ADC__RESOLUTION);
  pinMode(ADC_PIN, INPUT_ANALOG);

  Serial.begin(BAUD_RATE);
  delay(SERIAL_STABLE_DELAY);
  vSerialprint("\r\n==== End Node ADC Application ====\r\n",VLEVEL_H);

  SystemApp_Init();

  LoRaWAN_AppInit();

}

void loop() {
  // put your main code here, to run repeatedly:
  MX_LoRaWAN_Process();

}

void vSerialprint(char *pu8data, uint8_t u8Printlevel)
{
  #if (LOG_ENABLE == 1U)
  if(u8Printlevel >= DEBUG_LEVEL) {
      if(pu8data != NULL) {
          Serial.print(pu8data);
      } else {
          //no more action
      }
  } else {
      //no more action
  }
  #endif
}

void vGetRadioTxData(uint8_t *pu8Data, uint8_t *pu8size)
{
  if((pu8Data != NULL) && (pu8size != NULL)) {
      char cbuffer[UART_DATA_MAX_SIZE] = {0};
      uint8_t u8Index = 0U;
      uint16_t u16ADCSense = adc_read_value(ADC_PIN,ADC__RESOLUTION);
      uint16_t u16BatteryVolt =  __LL_ADC_CALC_DATA_TO_VOLTAGE(VREFINT_CAL_VREF, u16ADCSense, LL_ADC_RESOLUTION_12B);
      pu8Data[u8Index++] = (uint8_t)((u16BatteryVolt >> SHIFT_8) & 0xFFU);
      pu8Data[u8Index++] = (uint8_t)((u16BatteryVolt) & 0xFFU);
      *pu8size = u8Index;
      vSerialprint("\r\nBattery Voltage = ",VLEVEL_H);
      sprintf(cbuffer,"%04x",u16BatteryVolt);
      vSerialprint(cbuffer,VLEVEL_H);
      vSerialprint("\r\n",VLEVEL_H);
  } else {
      //no more action
  }
}

void vRadioRxData(uint8_t *pu8Data, uint8_t u8size)
{
  if(pu8Data != NULL) {
     uint8_t u8loop = 0U;
     char cbuffer[UART_DATA_MAX_SIZE] = {0};
     vSerialprint("Rx Data = ",VLEVEL_H);
     for(u8loop = 0U; u8loop < u8size; u8loop++) {
       (void)memset(cbuffer,0U,sizeof(cbuffer));
       sprintf(cbuffer,"%02x",pu8Data[u8loop]);
       vSerialprint(cbuffer,VLEVEL_H);
      }
      vSerialprint("\r\n",VLEVEL_H);
  } else {
      //no more action
  }
}

uint32_t u32GetLCRTime(void)
{
    return LCR_CMD_TIME;
}

uint32_t u32GetTDCTime(void)
{
    return APP_TX_DUTYCYCLE;
}

uint8_t u8GetIDKeyType(void)
{
    return STATIC_DEVICE_ID_KEY;
}

uint8_t u8GetActivationMode(void)
{
    return LORAWAN_DEFAULT_ACTIVATION_TYPE;
}

uint8_t *pu8GetDeviceID(void)
{
    static uint8_t pu8ID[] = DEVICE_ID;
    return pu8ID;
}

uint8_t *pu8GetAppKey(void)
{
    static uint8_t pu8key[] = APPLICATION_KEY;
    return pu8key;
}

uint8_t *pu8GetAppsKey(void)
{
    static uint8_t pu8key[] = APPLICATION_SESSION_KEY;
    return pu8key;
}

uint8_t *pu8GetNwksKey(void)
{
    static uint8_t pu8key[] = NETWORK_SESSION_KEY;
    return pu8key;
}
/***********************************************************************
 * END OF FILE
 ***********************************************************************/
