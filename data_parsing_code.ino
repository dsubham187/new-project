/*!
  * ----------------------------------------------------------------------
  * Copyright (c) 2022, System Level Solutions (India) Pvt. Ltd.
  * ----------------------------------------------------------------------
  *      Purpose:   This file is used to perform PWM operation
  *      Package:   STM32WLE5JCI
  *      File name: LoRaWan_Endnode_PWM.ino
  * ----------------------------------------------------------------------
  */


/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <LoRaSTM32WL_Lib.h>
#include <SoftwareSerial.h>
#include "./src/main.h"
#include "./src/sys_app.h"
#include "./src/lora_app.h"
#include "./src/LoRaLib.h"

#include "./src/stm32_seq.h"
#include "./src/stm32_timer.h"
#include "./src/utilities_def.h"
#include "./src/stm32_lpm.h"
#include "./src/sys_conf.h"

/***********************************************************************
 * CONSTANTS
 **********************************************************************/
#define  RADIO_TX_BUFFER_SIZE                      242U
#define  BAUD_RATE                                 115200U
#define  SERIAL_STABLE_DELAY                       500U
#define  MAX_VAL                                   16U
#define  LED_PIN                                   PB_1
#define  DEF_DUTYCYCLE                             10U  // in %
#define  PERIOD                                    10U // On+Off time in mSeconds
#define  PERCENATGE_CONV                           100U
#define  RESET                                     0U
#define  SET                                       1U
#define  FIRST_INDEX                               0U
#define  SECOND_INDEX                              1U
#define  DUTY_CYCLE_DWN_IND                        0x7EU
#define SLAVE_RESPONSE_TIMEOUT 3000


HardwareSerial Serial1(PA10, PB6);

/*!
 * Defines the application data transmission duty cycle. 300s, value in [ms].
 */
#define APP_TX_DUTYCYCLE                            30000

/*!
 * LoRaWAN default activation type
 */
#define LORAWAN_DEFAULT_ACTIVATION_TYPE             LORAWAN_ACTIVATION_TYPE_OTAA // or LORAWAN_ACTIVATION_TYPE_ABP

/*!
 * Time to send Link check request command
 */
#define LCR_CMD_TIME                                3600000    //3600000 seconds

/*!
 * STATIC_DEVICE_ID_KEY macro will be 1 if user want to use below IDs
 */
#define   STATIC_DEVICE_ID_KEY                      1U

/*!
 * Device ID
 */
#define   DEVICE_ID                                 {0x00,0x80,0xe1,0x15,0x00,0x2b,0x66,0x2f}//00 80 e1 15 00 2b 66 2f

/*!
 * Application key
 */
#define   APPLICATION_KEY                           {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c}// 2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c

/*!
 * Application session key
 */
#define   APPLICATION_SESSION_KEY                   {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c}

/*!
 * Network session key
 */
#define   NETWORK_SESSION_KEY                       {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c}

/***********************************************************************
 * GLOBAL VARIABLES
 **********************************************************************/


/*
  defining two byte array to storing the value
*/
byte values[1000];
byte values2[1000];
String recivedata="";



/*
*commands of modbus master to slave communication 
*/
//const byte data[]={0xAA,0x06,0x00,0xBA,0x00,0x01,0x70,0x34};//relay off
//const byte data[] = { 0xAA, 0x06, 0x00, 0xB9, 0x00, 0x00, 0x41, 0xF4 };  //relay on
//const byte data[]={0xAA,0x03,0x00,0x01,0x00,0x04,0x0C,0x12};//read command for EB KWH/1000, DG KWH/1000, Relay status, EB/DG status
const byte data[]={0xAA,0x03,0x00,0x1B,0x00,0x0A,0xAC,0x11};
//const byte data[]={0xAA,0x03,0x00,0x39,0x00,0x05,0x4C,0x1F};



/*
  Read function for getting the modbus response
*/
void read();




uint8_t u8RadioTxBuffer[RADIO_TX_BUFFER_SIZE] = {0xC8,0x54,0xAA,0x5A,0xBC,0x65};
uint8_t u8RadioTxBufferSize = 0x08U;
volatile uint32_t u32Ontime = 0U;
volatile uint32_t u32Offtime = 0U;
/***********************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES
 **********************************************************************/

/*!
 * \brief To update PWM duty cycle
 *
 * \param uint8_t Duty cycle
 *
 * \retval none
 */
void vUpdatePWMDutycycle(uint8_t u8DutyCycle);

/***********************************************************************
 * PUBLIC FUNCTIONS
 **********************************************************************/
void setup() {
  // put your setup code here, to run once:
  
  HAL_Init();
  SystemClock_Config();
  Serial.begin(BAUD_RATE);
  Serial1.begin(9600);

  delay(SERIAL_STABLE_DELAY);
  vSerialprint("\r\n==== End Node Application ====\r\n",VLEVEL_H);
  read();

  SystemApp_Init();

  LoRaWAN_AppInit();

  pinMode(LED_PIN,OUTPUT);
  vUpdatePWMDutycycle(DEF_DUTYCYCLE);

}

void loop() {

  
  // put your main code here, to run repeatedly:
  
  
  MX_LoRaWAN_Process();
  vGeneratePWM_LEDControl();

}

void vSerialprint(char *pu8data, uint8_t u8Printlevel)
{
  #if (LOG_ENABLE == 1U)
  if(u8Printlevel >= DEBUG_LEVEL) {
      if(pu8data != NULL) {
          Serial.print(pu8data);
      } else {
          //no more action
      }
  } else {
      //no more action
  }
  #endif
}

void vGetRadioTxData(uint8_t *pu8Data, uint8_t *pu8size)
{
  
  if((pu8Data != NULL) && (pu8size != NULL)) {
      (void)memcpy(pu8Data,u8RadioTxBuffer,u8RadioTxBufferSize);
      *pu8size = u8RadioTxBufferSize;
  } else {
      //no more action
  }
}

void vRadioRxData(uint8_t *pu8Data, uint8_t u8size)
{
  if(pu8Data != NULL) {
     uint8_t u8loop = 0U;
     char cbuffer[UART_DATA_MAX_SIZE] = {0};
     vSerialprint("Rx Data = ",VLEVEL_H);
     for(u8loop = 0U; u8loop < u8size; u8loop++) {
       (void)memset(cbuffer,0U,sizeof(cbuffer));
       sprintf(cbuffer,"%02x",pu8Data[u8loop]);
       vSerialprint(cbuffer,VLEVEL_H);
       //values2[u8loop]=byte(atoi(cbuffer));     
       recivedata+=cbuffer;  
      }
      /*for(int i=0;i<100;i++){
        Serial.println(values2[i]);
      }*/
      Serial.println(recivedata);

      vSerialprint("\r\n",VLEVEL_H);
      if(pu8Data[FIRST_INDEX] == DUTY_CYCLE_DWN_IND) {
        vUpdatePWMDutycycle(pu8Data[SECOND_INDEX]);
        vSerialprint("\r\n Duty Cycle Updated\r\n",VLEVEL_H);
      }
  } else {
      //no more action
  }
}

uint32_t u32GetLCRTime(void)
{
    return LCR_CMD_TIME;
}

uint32_t u32GetTDCTime(void)
{
    return APP_TX_DUTYCYCLE;
}

uint8_t u8GetIDKeyType(void)
{
    return STATIC_DEVICE_ID_KEY;
}

uint8_t u8GetActivationMode(void)
{
    return LORAWAN_DEFAULT_ACTIVATION_TYPE;
}

uint8_t *pu8GetDeviceID(void)
{
    static uint8_t pu8ID[] = DEVICE_ID;
    return pu8ID;
}

uint8_t *pu8GetAppKey(void)
{
    static uint8_t pu8key[] = APPLICATION_KEY;
    return pu8key;
}

uint8_t *pu8GetAppsKey(void)
{
    static uint8_t pu8key[] = APPLICATION_SESSION_KEY;
    return pu8key;
}

uint8_t *pu8GetNwksKey(void)
{
    static uint8_t pu8key[] = NETWORK_SESSION_KEY;
    return pu8key;
}

void vUpdatePWMDutycycle(uint8_t u8DutyCycle)
{
    u32Ontime = ((u8DutyCycle * PERIOD) / PERCENATGE_CONV);
    u32Offtime = PERIOD - u32Ontime;
}

void vGeneratePWM_LEDControl(void)
{
     digitalWrite(LED_PIN, SET);
     delay(u32Ontime);
     digitalWrite(LED_PIN, RESET);
     delay(u32Offtime);
}

void read() {
  char slaveResponseHex[30];
  int slaveResponseHexLen = 0;
  uint long requestStartTime = millis();
  int j = 0;

  if (Serial1.write(data, sizeof(data))) {                       //write the byte array data into slave modbus                  
    while (!Serial1.available() && (millis()-requestStartTime) < SLAVE_RESPONSE_TIMEOUT  ) {
      delay(1000);
      Serial.println("inside read while");
    }

    Serial.println("after read while");
    if (Serial1.available() > 0) {
      Serial.println("inside if");




      /* while(Serial1.available() > 0 || ((millis()-requestStartTime) < SLAVE_RESPONSE_TIMEOUT))
      {
        if(Serial1.available()> 0)
        {
          values[i++]= Serial1.read();
          Serial.println(values[i],HEX);
        }
      }*/


      for (int i = 0; i < 10000; i++) {                               //looping to get the each & every every modbus value untill crc
        if (Serial1.available() > 0) {
          values[i] = Serial1.read();
          Serial.println(values[i], HEX); 
          j++;
          Serial.println(i);
          Serial.println(j);
          Serial.println("##########################################################");
          
        }
      }
      Serial.println("-----------------------------------------------------------");
      for(int i=0;i<j;i++){                                   // storing the HEX values into u8RadioTxBuffersize
        u8RadioTxBuffer[i]=values[i];
        Serial.println(u8RadioTxBuffer[i],HEX);
      }
      /*for(int i=0;i<=j;i++){
          Serial.println(values2[i],HEX);
      }*/
      u8RadioTxBufferSize=(uint8_t)j;                         //stroring the size of the array
      delay(5000);

      Serial.println("End of Loop");
    }



    /*if(Serial1.available() > 0){

          for(int i=0;i<Serial1.;i++)
          
          // break;
         
    }
    /*for(byte i=0;i<7;i++){
    values[i] = Serial1.readbytes(slaveResponse,18);
    Serial.print(values[i],HEX);
    }*/
  }
}
/***********************************************************************
 * END OF FILE
 ***********************************************************************/
